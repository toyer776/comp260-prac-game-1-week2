﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector2 velocity;
	public float maxSpeed = 5.0f;
	public string horizontalAxis;
	public string verticalAxis;



	void Update() {
		Vector2 direction;
		direction.x = Input.GetAxis (horizontalAxis);
		direction.y = Input.GetAxis (verticalAxis);
		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);

	}
}
